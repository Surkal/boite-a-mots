import json
from urllib.parse import urlparse
import xml.etree.ElementTree as ET

from scrapy import Request
from scrapy.spiders import CrawlSpider, SitemapSpider
from django.core.exceptions import ObjectDoesNotExist

from scraper.items import Article, AnalyzedUrls


class Tools:

    def parser(self, response):
        webparser = self.extractor()
        try:
            # TODO: Maybe useless with IgnoreDuplicates middleware
            AnalyzedUrls.objects.get(url=response.url, analyzed=True)
            return None
        except ObjectDoesNotExist:
            pass

        item = Article()
        item['url'] = response.url
        item['website'] = urlparse(response.url).netloc

        try:
            item['website']
        except KeyError:
            return item

        # To catch all subdomains of some websites
        mult_websites = {
            'lexpress': 'www.lexpress.fr',
            'lefigaro': 'www.lefigaro.fr'
        }
        for k, v in mult_websites.items():
            if k in item['website']:
                item['website'] = v

        # Extracts title, date and article content from the web page
        for x in ('title', 'date', 'article'):
            if isinstance(webparser[item['website']][x], list):
                for y in webparser[item['website']][x]:
                    item[x] = response.css(y).extract_first()
                    if item[x] is not None or not item[x] == []:
                        break
            else:
                raw = response.css(webparser[item['website']][x])
                if x == 'article':
                    item[x] = raw.extract()
                else:
                    item[x] = raw.extract_first()

        return item

    def extractor(self):
        """Load the json file which contains key informations to scrape."""
        with open('/code/scraper/websites.json') as fp:
            return json.load(fp)


class RSSReader(CrawlSpider, Tools):

    name = 'rss'

    start_urls = ['http://www.vududroit.com/feed/', 'http://h16free.com/feed',
                  'http://www.anti-k.org/feed/', 'http://www.ipsn.eu/feed/',
                  'https://www.marianne.net/rss.xml', 'http://cafe-geo.net/feed/',
                  'http://www.lesinrocks.com/actualite/feed/',
                  'http://www.lenouveleconomiste.fr/feed/', 'http://lvsl.fr/feed',
                  'http://24matin.fr/feed/', 'http://www.al-kanz.org/feed/',
                  'https://lesbrindherbes.org/feed/', 'http://www.meta-media.fr/feed',
                  'http://www.voltairenet.org/spip.php?page=backend&id_secteur=1110&lang=fr',
                  'http://www.sceneweb.fr/rss/', 'http://rebellion-sre.fr/feed/',
                  'http://www.egaliteetreconciliation.fr/spip.php?page=backend',
                  'http://www.japoninfos.com/feed', 'https://rsf.org/fr/rss.xml',
                  'http://www.lemonde.fr/afrique/rss_full.xml',
                  'http://russeurope.hypotheses.org/feed',
                  'https://www.bastamag.net/spip.php?page=backend',
                  'https://el-manchar.com/feed/', 'http://www.filoche.net/feed/',
                  'http://www.geopolintel.fr/backend.php3',
                  'https://networkpointzero.wordpress.com/feed/',
                  'https://audelancelin.com/feed/', 'http://www.numerama.com/feed/',
                  'http://flux.consoglobe.com/consoglobe',
                  'https://prochetmoyen-orient.ch/feed/',
                  'https://ruptures-presse.fr/feed/', 'http://www.euractiv.fr/feed/',
                  'http://www.medelu.org/spip.php?page=backend',
                  'https://www.actu-environnement.com/flux/rss/environnement/',
                  'https://www.populationdata.net/feed/',
                  'http://www.letudiant.fr/educpros/rss.html',
                  'https://visegradpost.com/fr/feed/',
                  'http://www.frustrationlarevue.fr/feed/',
                  'https://www.eazylang.com/blog/index.php/feed/',
                  'http://communication-ccas.fr/journal/feed/',
                  'https://www.vice.com/fr/rss', 'http://fr.ign.com/feed.xml',
                  'http://vigile.quebec/spip.php?page=backend',
                  'http://www.01net.com/rss/info/flux-rss/flux-toutes-les-actualites/',
                  'https://iatranshumanisme.com/feed/',
                  'http://realitesroutieres.fr/feed/',
                  'https://theierecosmique.com/feed/',
                  'http://www.automobile-propre.com/feed/',
                  'http://boireetmanger.com/feed/', 'https://www.politis.fr/rss.xml',
                  'https://savoirsdhistoire.wordpress.com/feed/',
                  'http://feeds2.feedburner.com/LeJournalduGeek',
                  'https://www.contrepoints.org/feed',
                  'https://paris-luttes.info/spip.php?page=backend&lang=fr',
                  'http://www.metropolitiques.eu/spip.php?page=backend',
                  'https://www.espritsciencemetaphysiques.com/feed',
                  'http://feeds2.feedburner.com/banque/actualite',
                  'https://rebellyon.info/spip.php?page=backend',
                  'https://www.lobservateurdebeauvais.fr/feed/',
                  'https://www.troubles-bipolaires.com/feed/',
                  'https://alaingrandjean.fr/feed/?cat=-463',
                  'https://lejournal.cnrs.fr/rss',
                  'http://www.pausefoot.com/feed/',
                  'https://scinfolex.com/feed/',
                  'http://lesociologue.com/feed/',
                  'http://www.lelanceur.fr/feed/',
                  'http://japanization.org/feed/',
                  'http://lepharmachien.com/feed/',
                  'http://www.inrap.fr/rss.xml',
                  'http://www.laboiteverte.fr/feed/',
                  'http://www.lemondedelenergie.com/feed/',
                  'https://www.initiative-communiste.fr/feed/',
                  'https://www.blogdumoderateur.com/feed/',
                  'http://lafriqueadulte.com/feed/',
                  'https://vmc.camp/feed/',
                  'https://framablog.org/feed/',
                  'https://usbeketrica.com/rss',
                  'https://www.revue-ballast.fr/feed/',
                  'https://sciencetonnante.wordpress.com/feed/',
                  'https://le-bon-sens.com/feed/',
                  'https://fr.metrotime.be/feed/',
                  'http://www.zakweli.com/feed/',
                  'https://www.bellica.fr/feed/',
                  'https://www.sciencesetavenir.fr/rss.xml',
                  'https://www.ritimo.org/spip.php?page=backend',
                  'http://www.mondesolidaire72.fr/spip.php?page=backend',
                  'http://feeds.feedburner.com/les-crises-fr',
                  'http://bfmbusiness.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/',
                  'https://www.terre-net.fr/actualite-agriculture.html',
                  'https://brunoadrie.wordpress.com/feed/',
                  'https://www.monde-diplomatique.fr/rss/',
                  'http://menace-theoriste.fr/feed/',
                  'https://pluszaine.wordpress.com/feed/',
                  'https://www.usinenouvelle.com/rss/',
                  'https://cryptonaute.fr/feed/',
                  'https://factuel.afp.com/rss.xml',
                  'https://rapportsdeforce.fr/feed',
    ]

    allowed_domains = [
        'boireetmanger.com', 'revue-ballast.fr', 'alaingrandjean.fr', 'ign.com',
        'rebellyon.info', 'ruptures-presse.fr', 'boireetmanger.com', 'vmc.camp',
        'lesbrindherbes.org', 'pausefoot.com', 'paris-luttes.info', '01net.com',
        'lobservateurdebeauvais.fr', 'inrap.fr', 'geopolintel.fr', 'bellica.fr',
        'populationdata.net', 'audelancelin.com', 'scinfolex.com', 'ritimo.org',
        'vududroit.com', 'anti-k.org', 'vice.com', 'h16free.com', 'cbanque.com',
        'iatranshumanisme.com', 'marianne.net', 'lesinrocks.com', 'euractiv.fr',
        'russeurope.hypotheses.org', 'japoninfos.com', 'ipsn.eu', 'filoche.net',
        'le-bon-sens.com', 'metropolitiques.eu', 'metrotime.be', 'lelanceur.fr',
        'sceneweb.fr', 'cafe-geo.net', 'actu-environnement.com', 'bastamag.net',
        'el-manchar.com', 'automobile-propre.com', 'bfmtv.com', 'les-crises.fr',
        'communication-ccas.fr', 'visegradpost.com', 'cnrs.fr', 'framablog.org',
        'contrepoints.org', 'consoglobe.com', 'terre-net.fr', 'usbeketrica.com',
        'networkpointzero.wordpress.com','lesociologue.com', 'japanization.org',
        'letudiant.fr', 'eazylang.com', 'journaldugeek.com', 'rebellion-sre.fr',
        'initiative-communiste.fr', 'troubles-bipolaires', 'lafriqueadulte.com',
        'numerama.com', 'metrotime.be', 'meta-media.fr', 'realitesroutieres.fr',
        'prochetmoyen-orient.ch', 'theierecosmique.com', 'blogdumoderateur.com',
        'lemondedelenergie.com', 'mondesolidaire72.fr', 'frustrationlarevue.fr',
        'lemonde.fr', 'al-kanz.org', 'politis.fr', 'egaliteetreconciliation.fr',
        'medelu.org', 'monde-diplomatique.fr', 'savoirsdhistoire.wordpress.com',
        'lvsl.fr', 'rsf.org', 'wordpress.com', 'espritsciencemetaphysiques.com',
        'lepharmachien.com', 'sciencesetavenir.fr', 'menace-theoriste.fr',
        'zakweli.com', 'lenouveleconomiste.fr', 'voltairenet.org',
        'usinenouvelle.com', 'cryptonaute.fr', 'afp.com',
        'rapportsdeforce.fr',
    ]

    def parse(self, response):
        webparser = self.extractor()
        website = webparser[response.meta['download_slot']]

        try:
            typ = website['type']
            selector = website['selector']
        except KeyError:
            typ, selector = 'css', 'link::text'

        if typ == 'css':
            links = response.css(selector).extract()
        else:
            root = ET.fromstring(response.text)
            results = root.findall('.//link')
            links = list(map(lambda n: n.text, results))

        for link in links:
            yield Request(url=link, callback=self.parser)


class NewsReader(SitemapSpider, Tools):

    name = "news"

    sitemap_urls = [
        'https://actu17.fr/news-sitemap.xml',
        'http://www.bfmtv.com/sitemap_news.xml',
        'http://www.lemonde.fr/sitemap_news.xml',
        'http://www.lepoint.fr/sitemap-news.xml',
        'https://fr.sputniknews.com/sitemap.xml',
        'http://www.varmatin.com/googlenews.xml',
        'http://www.journaldequebec.com/news.xml',
        'http://www.sudouest.fr/sitemap-news.xml',
        'http://www.nicematin.com/googlenews.xml',
        'http://www.lefigaro.fr/sitemap_actu.xml',
        'http://www.atlantico.fr/sitemap-news.xml',
        'http://cheekmagazine.fr/sitemap-news.xml',
        'http://www.frenchweb.fr/sitemap-news.xml',
		'http://www.liberation.fr/sitemap_news.xml',
        'http://www.lexpress.fr/sitemap_actu_1.xml',
        'http://www.ouest-france.fr/googlenews.xml',
        'http://www.leparisien.fr/sitemap_news_1.xml',
        'http://www.letelegramme.fr/sitemap_news.xml',
        'http://www.la-croix.com/sitemap/google-news',
        'https://www.franceinter.fr/sitemap-news.xml',
        'https://unodieuxconnard.com/news-sitemap.xml',
        'https://www.franceculture.fr/sitemap-news.xml',
        'http://theconversation.com/fr/sitemap_news.xml',
        'http://ici.radio-canada.ca/SitemapNouvelles.xml',
        'https://www.nextinpact.com/actualites-sitemap.xml',
        'http://www.jeuneafrique.com/sitemap-googlenews.xml',
        'http://fr.euronews.com/sitemaps/fr/latest-news.xml',
        'https://www.generation-nt.com/sitemap_actualites.xml',
        'http://www.lavoixdunord.fr/sites/default/files/sitemaps/sitemapdaily-0.xml',
        'http://www.lavoixdunord.fr/sites/default/files/sitemaps/sitemapmonthly-0.xml',
    ]

    sitemap_rules = [
        ('actu17\.fr/\w+', 'parser'),
        ('bfmtv\.com/\w+', 'parser'),
        ('lepoint\.fr/\w+', 'parser'),
        ('lemonde\.fr/\w+', 'parser'),
        ('lefigaro\.fr/\w+', 'parser'),
        ('lexpress\.fr/\w+', 'parser'),
        ('sudouest\.fr/\w+', 'parser'),
        ('atlantico\.fr/\w+', 'parser'),
        ('euronews\.com/\w+', 'parser'),
        ('frenchweb\.fr/\w+', 'parser'),
        ('varmatin\.com/\w+', 'parser'),
        ('la\-croix\.com/\w+', 'parser'),
        ('liberation\.fr/\w+', 'parser'),
        ('leparisien\.fr/\w+', 'parser'),
        ('nicematin\.com/\w+', 'parser'),
        ('franceinter\.fr/\w+', 'parser'),
        ('nextinpact\.com/\w+', 'parser'),
        ('journaldequebec\.com', 'parser'),
        ('lavoixdunord\.fr/\w+', 'parser'),
        ('letelegramme\.fr/\w+', 'parser'),
        ('ouest-france\.fr/\w+', 'parser'),
        ('radio-canada\.ca/\w+', 'parser'),
        ('sputniknews\.com/\w+', 'parser'),
        ('unodieuxconnard\.com', 'parser'),
        ('cheekmagazine\.fr/\w+', 'parser'),
        ('franceculture\.fr/\w+', 'parser'),
        ('jeuneafrique\.com/\w+', 'parser'),
        ('generation\-nt\.com/\w+', 'parser'),
        ('theconversation\.com/\w+', 'parser'),
    ]
