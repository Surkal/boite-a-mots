import re
from urllib.parse import urlsplit

from django import template
from django.urls import reverse

from words.utils import get_sortkey
from words.models import Quote, Quotes, Word


register = template.Library()

def _bold_title(sentence, title):
    # TODO: could be incorporated in 'bold_query' function
    pattern = re.compile(title, re.IGNORECASE)
    return pattern.sub(f'<strong>{title}</strong>', sentence)

@register.filter
def quote_decorator(value, quotes):
    """
    Bold the new word (page title) in the quote (value).
    Add hyperlink to words in the quote and database.
    """
    x = Quotes.objects.get(id=quotes)
    value = _bold_title(value, x.word.word)

    words = set()
    for y in Quotes.objects.filter(quote=x.quote, word__checked=True):
        words.add(y.word.word)
    words.remove(x.word.word)

    for word in words:
        url = reverse('word', kwargs={'title': word})
        new = f'<a href="{url}">{word}</a>'
        value = value.replace(word, new)

    return value

@register.filter
def bold_query(value, query):
    """
    Bold the query element(s) in the quote.
    Handles case and accentuation.
    """
    tag = '<strong>'
    tag_ = '</strong>'
    query = query.split(' ')
    for q in query:
        count = 0
        value_ = value.lower()
        value_ = get_sortkey(value_)
        pattern = re.compile(q)
        for x in pattern.finditer(value_):
            a = x.start() + len(tag+tag_) * count
            b = x.end() + len(tag+tag_) * count + len(tag)
            value = value[:a] + tag + value[a:]
            value = value[:b] + tag_ + value[b:]
            count += 1
    return value

@register.filter
def quote_links(value, quote_id):
    for x in Quotes.objects.filter(quote__id=quote_id):
        word = x.word.word
        url = reverse('word', kwargs={'title': word})
        new = f'<a href="{url}">{word}</a>'
        value = value.replace(word, new)
    return value

@register.filter
def clean_site(value):
    value = urlsplit(value).netloc
    if value[:4] == 'www.':
        return value[4:]
    return value
