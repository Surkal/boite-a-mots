from random import randrange

from faker import Factory
from django.urls import reverse
from django.test import TestCase, Client

from .tests import PopulatingTestCase
from words.views import IndexView
from words.models import Word, Quote, Quotes


class IndexTest(TestCase):
    def setUp(self):
        super().setUp()
        self.fake = Factory.create('fr_FR')

    def test_general_view(self):
        self.assertEqual(self.client.get(reverse('index')).status_code, 200)
        self.assertEqual(self.client.get(f'{reverse("index")}?show=popular').status_code, 200)
        self.assertTrue(all(self.client.get(f'{reverse("index")}?show=popular&period=' + randrange(6, 99)).status_code, 200) for x in range(10))

    def test_context(self):
        r = self.client.get(reverse('index'))
        self.assertEqual(len(r.context['object_list']), Word.objects.all().count())
        self.assertFalse(r.context['is_paginated'])

    def test_context_paginated(self):
        # Test for more than 240 words (pagination by default)
        q = Quote.objects.create(quote=self.fake.sentence(), title=self.fake.sentence(), url=self.fake.url())
        words = set(self.fake.user_name() for _ in range(500))
        for word in words:
            w = Word.objects.create(word=word, checked=True, sortkey='zzz')
            Quotes.objects.create(quote=q, word=w)
        r_ = self.client.get(reverse('index'))
        self.assertLess(len(r_.context['object_list']), Word.objects.all().count())
        self.assertTrue(r_.context['is_paginated'])
