from django.conf.urls import url
from django.views.decorators.cache import cache_page

from .views import AdvancedSearchView, IndexView, RandomWordView, \
                   SearchQuoteView, SearchView, StatsView, WordView


urlpatterns = [
    url(r'^index$', cache_page(86400)(IndexView.as_view()), name='index'),
    url(r'^stats$', cache_page(86400)(StatsView.as_view()), name='stats'),
    url(r'^random$', RandomWordView.as_view(), name='random'),
    url(r'^advanced_search$', cache_page(86400)(AdvancedSearchView.as_view()), name='advanced_search'),
    url(r'^search$', cache_page(3600)(SearchView.as_view()), name='search'),
    url(r'^searchquote$', cache_page(3600)(SearchQuoteView.as_view()), name='search_quote'),
    url(r'^(?P<title>\w+)$', cache_page(3600)(WordView.as_view()), name='word'),
]
