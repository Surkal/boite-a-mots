from django.urls import reverse
from django.test import TestCase

from faker import Factory

from words.models import Quote, Quotes, Word
from words.templatetags.my_filters import clean_site,\
                                          bold_query,\
                                          _bold_title,\
                                          quote_decorator,\
                                          quote_links


class FilterTest(TestCase):

    def setUp(self):
        self.fake = Factory.create('fr_FR')
        a = Word.objects.create(word="world", sortkey="world", checked=True)
        b = Quote.objects.create(quote="Hello world!", title=self.fake.sentence(), url=self.fake.url())
        l = Word.objects.create(word="ladies", sortkey="ladies", checked=True)
        g = Word.objects.create(word="gentlemen", sortkey="gentlemen", checked=True)
        q = Quote.objects.create(quote="Hello ladies and gentlemen!", title=self.fake.sentence(), url=self.fake.url())
        Quotes.objects.create(word=a, quote=b)
        Quotes.objects.create(word=l, quote=q)
        Quotes.objects.create(word=g, quote=q)

    def test_quote_decorator(self):
        url = reverse("word", kwargs={"title": "ladies"})
        world = Quotes.objects.get(word__word="world")
        self.assertEqual(quote_decorator("Hello world!", world.id), "Hello <strong>world</strong>!")
        self.assertEqual(quote_decorator("Hello world double world!", world.id), "Hello <strong>world</strong> double <strong>world</strong>!")
        gentlemen = Quotes.objects.get(word__word="gentlemen")
        self.assertEqual(quote_decorator("Hello ladies and gentlemen!", gentlemen.id), f'Hello <a href="{url}">ladies</a> and <strong>gentlemen</strong>!')

    def test_bold_title(self):
        self.assertEqual(_bold_title('hello world!', 'world'), 'hello <strong>world</strong>!')
        self.assertEqual(_bold_title('hello world!', 'hello'), '<strong>hello</strong> world!')
        self.assertEqual(_bold_title('hello world and hello again.', 'hello'), '<strong>hello</strong> world and <strong>hello</strong> again.')

    def test_bold_query(self):
        self.assertEqual(bold_query('Hello world!', 'world'), 'Hello <strong>world</strong>!')
        self.assertEqual(bold_query('Hello world!', 'hello'), '<strong>Hello</strong> world!')
        self.assertEqual(bold_query('Hello world and hello again.', 'hello'), '<strong>Hello</strong> world and <strong>hello</strong> again.')
        self.assertEqual(bold_query('Hello world hey guys!', 'world guys'), 'Hello <strong>world</strong> hey <strong>guys</strong>!')
        self.assertEqual(bold_query('Hey guys hello world.', 'world guys'), 'Hey <strong>guys</strong> hello <strong>world</strong>.')

    def test_quote_links(self):
        quote = Quote.objects.get(quote="Hello ladies and gentlemen!")
        quote_id = quote.id
        self.assertEqual(quote_links(quote.quote, quote.id), 'Hello <a href="/words/ladies">ladies</a> and <a href="/words/gentlemen">gentlemen</a>!')

    def test_clean_site(self):
        self.assertEqual(clean_site('http://www.example.com'), 'example.com')
        self.assertEqual(clean_site('http://example.com'), 'example.com')
