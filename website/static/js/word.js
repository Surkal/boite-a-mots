$(function () {

  $("document").ready(function() {
    $("#box-view").on("click", ParamBox);
  });

  function ParamBox(evt) {
    $("#advanced-param").slideToggle('slow', function () {
      if ($(this).is(":visible")) {
        $("#box-view").text("Enrouler ▲");
      } else {
        $("#box-view").text("Dérouler ▼");
      }
    });
  }

});
