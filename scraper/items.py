import scrapy
from scrapy_djangoitem import DjangoItem

from words.models import AnalyzedUrls


class UrlsAnalyzed(DjangoItem):
    django_model = AnalyzedUrls


class Article(scrapy.Item):
    article = scrapy.Field()
    date    = scrapy.Field()
    quote   = scrapy.Field()
    quotes  = scrapy.Field()
    resume  = scrapy.Field()
    title   = scrapy.Field()
    url     = scrapy.Field()
    website = scrapy.Field()
    word    = scrapy.Field()
