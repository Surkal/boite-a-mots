import os
import re
import sys
import pytz
from time import gmtime, strftime
from datetime import datetime as dt

import nltk
from bs4 import BeautifulSoup
from django.db.models import F
from scrapy.conf import settings
from nltk.corpus import stopwords
from nltk import wordpunct_tokenize
from django.db import IntegrityError
from scrapy.exceptions import DropItem
from django.core.exceptions import ObjectDoesNotExist

from scraper.utils import get_sortkey
from scraper.text_analysis import ExistingWord
from words.models import AnalyzedUrls, Word, Quote, Quotes


class DatePipeline(object):
    """
    Transform the scraped date into datetime format.
    If the date is older than the limit date (cf settings), the page is dropped.
    """
    def __init__(self, date_limit):
        self.date_limit = dt.strptime(date_limit, '%y-%m-%d%z')

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        date_limit = settings.get('DATE_LIMIT')
        return cls(date_limit)

    def process_item(self, item, spider):
        if not item['date']:
            raise DropItem('Date not valid')
        d = item['date']
        d = d.replace(':', '')
        expr = ['%Y-%m-%dT%H%M%S%z',
                '%Y-%m-%dT%H%M%S',
                '%Y-%m-%dT%H%M%SZ',
                '%d-%m-%YT%H%M%SZ%z',
                '%Y-%m-%dT%H%M',
                '%Y-%m-%dT%H%M%SZ%z',
                '%d/%m/%Y',
                '%Y-%m-%d',
                '%Y/%m/%d',
                '%Y-%m-%dCEST%H%M%S%z',
                '%Y-%m-%dT%H%M%z',
                '%Y-%m-%d %H%M%S',
                '%Y%m%d %H%M',
                '%d/%m/%Y %H%M%S %z',
                '%d.%m.%Y',
                '%d/%m/%y %Hh%M ',
                '%Y-%m-%dT%H%M%S.000z',
        ]
        for x in expr:
            try:
                d = dt.strptime(d, x)
                break
            except ValueError:
                pass
        if not isinstance(d, dt):
            if re.match('\d{10}', d).group() == d:
                d = strftime("%Y-%m-%dT%H:%M:%S%z", gmtime(int(d)))
                d = dt.strptime(d, "%Y-%m-%dT%H:%M:%S%z")
        tz = pytz.timezone('Europe/Paris')
        if not d.tzinfo:
            d = d.replace(tzinfo=tz)
        item['date'] = d

        if self.date_limit > item['date']:
            raise DropItem("Article too old.")
        return item


class TitlePipeline(object):
    """
    Clean the title for some websites.
    Drop the page if there is no title.
    """
    def process_item(self, item, spider):
        if not item['title']:
            raise DropItem("No title.")
        return item


class TextPipeline(object):
    """
    Extract and clean the text from html attributes.
    """
    def process_item(self, item, spider):
        if not item['article']:
            raise DropItem('No text.')

        item['quotes'] = []
        for paragraphe in item['article']:
            soup = BeautifulSoup(paragraphe, 'html.parser')
            paragraphe = nltk.PunktSentenceTokenizer()
            item['quotes'] += paragraphe.sentences_from_text(soup.get_text())
        return item


class LanguagePipeline(object):
    """
    Determine the text language.
    Drop the page if not french.
    """
    def process_item(self, item, spider):
        texte = ''
        for x in item['quotes']:
            texte += x + ' '
        language = self.detect_language(texte)
        if not language == 'french':
            z = AnalyzedUrls.objects.select_for_update().filter(url=item['url']).update(language=language)
            if z == 0:
                AnalyzedUrls.objects.create(url=item['url'], language=language)
            raise DropItem("Not french language.")
        return item

    def detect_language(self, text):
        """Detect the text language."""
        languages_ratios = {}
        tokens = wordpunct_tokenize(text)
        words = [word.lower() for word in tokens]

        for language in stopwords.fileids():
            stopwords_set = set(stopwords.words(language))
            words_set = set(words)
            common_elements = words_set.intersection(stopwords_set)
            languages_ratios[language] = len(common_elements)
        return max(languages_ratios, key=languages_ratios.get)


class AnalysePipeline(object):
    """Analyze all the words of the text."""
    def process_item(self, item, spider):
        stopwords = self.useless_words('french')
        ex = ExistingWord(stopwords)
        for sentence in item['quotes']:
            words = self._split_words(sentence)
            for word in words:
                if not ex.is_word(sentence, words, word):
                    self.save_word(item, sentence, word)
        return item

    def _split_words(self, sentence):
        return nltk.word_tokenize(sentence)

    def useless_words(self, lang):
        """Exclude the most common words."""
        return set(nltk.corpus.stopwords.words(lang))

    def save_word(self, item, sentence, word):
        """
        Save the neologism found in the database
        with the sentence, title, date and url.
        """
        sortkey = get_sortkey(word)
        try:
            wd = Word.objects.create(word=word,
                                     sortkey=sortkey,
                                     first=item['date']
            )
        except IntegrityError:
            wd = Word.objects.get(word=word)
        try:
            qt = Quote.objects.get(quote=sentence)
        except ObjectDoesNotExist:
            qt = Quote.objects.create(quote = sentence,
                                      title = item['title'],
                                      hour  = item['date'],
                                      url   = item['url'],
            )
        try:
            Quotes.objects.create(word=wd, quote=qt)
        except IntegrityError:
            return None
        self._update_first_entry_date(word, wd, qt)
        self._add_quote(word, wd)

    def _update_first_entry_date(self, word, wd, qt):
        """
        Determine if the new quote of a word is older than the oldest one.
        Update the oldest quote date.
        """
        new = Quotes.objects.get(word=wd, quote=qt).quote.hour
        if not new < Word.objects.get(word=word).first:
            return None
        wd.first = new
        wd.save()

    def _add_quote(self, word, wd):
        """Number of quotes increased by 1, incrementes 'occurr'."""
        wd.occurr = Quotes.objects.filter(word__word=word).count()
        wd.save()


class SaveLinkPipeline(object):
    """Add the page to analyzed pages database."""
    def process_item(self, item, spider):
        try:
            AnalyzedUrls.objects.get(url=item['url'])
            AnalyzedUrls.objects.select_for_update().filter(url=item['url']).update(analyzed=True)
        except ObjectDoesNotExist:
            AnalyzedUrls.objects.create(url=item['url'], date=item['date'], analyzed=True)
