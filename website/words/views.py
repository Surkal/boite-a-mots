import re
import difflib
import datetime
import unicodedata
from datetime import timedelta
from urllib.parse import urlsplit

from django.urls import reverse
from django.db import connection
from django.utils import timezone
from django.db.models import Q, Count
from django.utils.timezone import now
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django.views.generic.base import RedirectView
from django.views.generic.detail import DetailView
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404
from django.views.generic.detail import SingleObjectMixin

from visitors.models import Visitor
from words.utils import get_sortkey
from words.similar import flexions, similar_words
from words.models import Word, Quote, Quotes, AnalyzedUrls
from words.forms import QuotesSortForm, AdvancedSearchForm, ExampleSearchForm


class AdvancedSearchView(FormView):
    template_name = 'words/search_form.html'
    form_class = AdvancedSearchForm
    form_class_ex = ExampleSearchForm

    def get_context_data(self, *args, **kwargs):
        context = super(AdvancedSearchView, self).get_context_data(**kwargs)
        context['searchform'] = self.form_class
        context['searchformex'] = self.form_class_ex
        return context


class IndexView(ListView):
    template_name = 'words/index.html'
    paginate_by = 240

    def get(self, request, *args, **kwargs):
        try:
            self.period = int(request.GET.get('period'))
        except (TypeError, AttributeError):
            self.period = 7
        self.show = request.GET.get('show')
        return super(IndexView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        if not self.show == 'popular':
            return Word.objects.filter(checked=True)
        today = timezone.now()
        start_date = today + datetime.timedelta(days=self.period * -1)
        words_in_range = Quotes.objects.select_related()\
                                       .filter(quote__hour__range=(start_date, today))\
                                       .order_by('word')
        self.queryset = words_in_range.values('word__word')\
                                      .filter(word__checked=True)\
                                      .annotate(count=Count('word'))\
                                      .order_by('-count')
        return self.queryset


class RandomWordView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        x = Quotes.objects.filter(word__checked=True).order_by('?').first()
        return reverse('word', kwargs={'title': x.word.word})


class SearchView(SingleObjectMixin, ListView):
    model = Word
    template_name = 'words/search.html'
    paginate_by = 60

    def dispatch(self, *args, **kwargs):
        return super(SearchView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = request.GET
        if not data:
            raise Http404('No query')
        simple_search = False
        try:
            query = data['q'].lower()
            simple_search = True
        except KeyError:
            pass
        if simple_search:
            try:
                Word.objects.get(word=query, checked=True)
                return HttpResponseRedirect(reverse('word', kwargs={'title': query}))
            except ObjectDoesNotExist:
                pass
            query_sortkey = get_sortkey(query)
            #self.description = ['similaires à <i>' + query + '</i>']
            s = Word.objects.filter(sortkey=query_sortkey, checked=True)
            if len(s) == 1:
                # Unique word matching query_sortkey in database
                return HttpResponseRedirect(reverse('word', kwargs={'title': s.first().word}))
            elif len(s) > 1:
                self.object = s
                self.description = ['très similaires à <i>' + query + '</i>']
            else:
                self.object = []
                all_words = Word.objects.filter(checked=True).values_list('word', flat=True)
                suggested_words = difflib.get_close_matches(query, all_words, n=100)
                for w in suggested_words:
                    self.object.append(Word.objects.get(word=w))
                if len(self.object) == 1:
                    self.description = ['similaire à <i>' + query + '</i>']
                else:
                    self.description = ['similaires à <i>' + query + '</i>']

        else:
            self.description = []
            start = get_sortkey(data['start'])
            end = get_sortkey(data['end'])
            contain = get_sortkey(data['contain'])
            self.object = Word.objects.filter(sortkey__startswith=start)\
                                      .filter(sortkey__endswith=end)\
                                      .filter(sortkey__contains=contain)
            if start:
                if len(self.object) == 1:
                    self.description.append('commençant par <i>' + data['start'] + '</i>')
                else:
                    self.description.append('commençants par <i>' + data['start'] + '</i>')
            if end:
                if len(self.object) == 1:
                    self.description.append('finissant par <i>' + data['end'] + '</i>')
                else:
                    self.description.append('finissants par <i>' + data['end'] + '</i>')
            if contain:
                if len(self.object) == 1:
                    self.description.append('contenant <i>' + data['contain'] + '</i>')
                else:
                    self.description.append('contenants <i>' + data['contain'] + '</i>')
        self.data = data
        return super(SearchView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['suggested_words'] = self.object
        context['description'] = self.description
        context['queries'] = self.data
        return context

    def get_queryset(self):
        return self.object

    def get_paginate_by(self, queryset):
        """
        User can customize the number of results per page (paginate_by) in
        the URL between 5 and 500.
        """
        try:
            pagesize = int(self.request.GET.get('paginateby'))
        except (ValueError, TypeError):
            pagesize = self.paginate_by
        if 4 > pagesize < 501:
            pagesize = self.paginate_by
        return pagesize


class SearchQuoteView(ListView):
    """View to search string among examples."""
    model = Quote
    paginate_by = 10
    template_name = "words/search_quote.html"

    def _normalize_query(self, query_string):
        findterms = re.compile(r'"([^"]+)"|(\S+)').findall
        normspace = re.compile(r'\s{2,}').sub
        return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]

    def _get_query(self, query_string):
        query = None
        terms = self._normalize_query(query_string)
        for term in terms:
            query = Q(**{"quotes__quote__quote__icontains": term, "quotes__word__checked": True})
        return query

    def get(self, request, *args, **kwargs):
        if not request.GET.get('w'):
            raise Http404('No query')
        self.query_string = request.GET.get('w')
        self.entry_query = self._get_query(self.query_string)
        return super(SearchQuoteView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        return Quote.objects.filter(self.entry_query)


    def get_context_data(self, **kwargs):
        context = super(SearchQuoteView, self).get_context_data(**kwargs)
        context['query'] = self.query_string
        return context


class StatsView(ListView):
    template_name = "words/stats.html"
    queryset = Word  # Useless but required

    def get(self, request, *args, **kwargs):
        today = timezone.now()
        period = 7  # Period by default
        if request.GET.get('period'):
            period = int(request.GET.get('period'))
        start_date = today + datetime.timedelta(days=period * -1)
        words_in_range = Quotes.objects.select_related()\
                                       .filter(word__checked=True)\
                                       .filter(quote__hour__range=(start_date, today))\
                                       .order_by('quote__hour')
        self.popular_words = words_in_range.values('word__word')\
                                           .filter(word__checked=True)\
                                           .annotate(count = Count('word__word'))\
                                           .order_by('-count')[:30]
        self.last_words = Word.objects.filter(checked=True).order_by('-first')[:36]
        self.analyzed_urls = self._quick_urls_count('words_analyzedurls')
        return super(StatsView, self).get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(StatsView, self).get_context_data(**kwargs)
        context['words'] = Word.objects.filter(checked=True).count()
        context['quotes'] = Quotes.objects.filter(word__checked=True).count()
        context['websites'] = self._websites_nb()
        context['popular_words'] = self.popular_words
        context['last_new_words'] = self.last_words
        context['unique_visitors'] = Visitor.objects.count()
        context['analyzed_urls'] = self.analyzed_urls
        return context

    def _websites_nb(self):
        websites = set()
        for x in Quote.objects.all():
            websites.add(urlsplit(x.url).netloc)
        return len(websites)

    def _quick_urls_count(self, table):
        """Quick (approximately) count of analyzed urls."""
        with connection.cursor() as cursor:
            cursor.execute(f"""SELECT reltuples FROM pg_class WHERE relname = '{table}';""")
            row = cursor.fetchone()
        return int(row[0])


class WordView(SingleObjectMixin, ListView):
    form_class = QuotesSortForm(initial={'pagesize': '10'})
    template_name = 'words/word.html'
    paginate_by = 10

    def get(self, request, *args, **kwargs):
        if request.GET.get('pagesize'):
            size = request.GET.get('pagesize')
            self.form_class = QuotesSortForm(initial={'pagesize': size})
        self.value = '-quote__hour'
        sort = {'desc': 'quote__hour', 'asc': '-quote__hour', 'rand': '?'}
        for key_, value in sort.items():
            if request.GET.get('quotesort') == key_:
                self.value = value
        return super(WordView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        self.title = get_object_or_404(Word, word=self.kwargs['title'])
        if not self.title.checked:
            raise Http404('Not checked word')
        self.object = Quotes.objects.filter(word__word=self.title).order_by(self.value)
        return self.object

    def get_context_data(self, **kwargs):
        context = super(WordView, self).get_context_data(**kwargs)
        context['word'] = self.object
        context['title'] = self.title
        context['flexions'] = flexions(self.title.word)
        context['similar'] = similar_words(self.title)
        context['quotesform'] = self.form_class
        return context

    def get_paginate_by(self, queryset):
        """
        User can customize the number of results per page (paginate_by) in
        the URL between 5 and 50.
        """
        try:
            pagesize = int(self.request.GET.get('pagesize'))
        except (ValueError, TypeError):
            pagesize = self.paginate_by
        if 4 > pagesize < 51:
            pagesize = self.paginate_by
        return pagesize
