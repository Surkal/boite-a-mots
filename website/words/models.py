from datetime import datetime as dt

import django.utils.timezone
from django.db import models
from django.urls import reverse


class Word(models.Model):
    word = models.CharField(unique=True, max_length=50)
    occurr = models.IntegerField(default=0)
    checked = models.BooleanField(default=False)
    sortkey = models.CharField(max_length=50)
    first = models.DateTimeField(default=django.utils.timezone.now)

    class Meta:
        ordering = ['sortkey']

    def __str__(self):
        return self.word

    def get_absolute_url(self):
        return '/words/' + self.word


class Quote(models.Model):
    quote = models.TextField(unique=True)
    title = models.CharField(max_length=200)
    hour = models.DateTimeField(default=django.utils.timezone.now)
    url = models.URLField(max_length=250)

    class Meta:
        ordering = ['-hour']

    def __str__(self):
        return self.quote

    @property
    def date(self):
        return dt.strftime(self.hour, '%Y-%m-%d')


class Quotes(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    quote = models.ForeignKey(Quote, on_delete=models.CASCADE)

    class Meta:
        ordering = ['quote__hour']
        unique_together = ('word', 'quote')

    def __str__(self):
        return self.quote.quote


class FrenchWords(models.Model):
    word = models.CharField(max_length=100, unique=True)

    class Meta:
        ordering = ['word']

    def __str__(self):
        return self.word


class AnalyzedUrls(models.Model):
    url = models.URLField(unique=True)
    analyzed = models.BooleanField(default=True)
    language = models.CharField(max_length=30, default="french")
    date = models.DateField(default=django.utils.timezone.now)

    def __str__(self):
        return self.url
