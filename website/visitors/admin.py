from django.contrib import admin

from visitors.models import Visitor, PageView


class PageViewInline(admin.TabularInline):
    model = PageView
    readonly_fields = ('url', 'referer',)

    def get_user_agent(self, obj):
        return obj.visitor.user_agent


class VisitorAdmin(admin.ModelAdmin):
    date_hierarchy = 'start_time'

    list_display = ('start_time', 'session_key', 'get_country_name', 'ip_address', 'user_agent',)

    inlines = [
        PageViewInline,
    ]

    def get_country_name(self, obj):
        return obj.country_name


class PageViewAdmin(admin.ModelAdmin):
    date_hierarchy = 'view_time'

    list_display = ('view_time', 'url', 'get_ip_address', 'get_user_agent', 'get_session_key', 'status_code',)

    def get_ip_address(self, obj):
        return obj.visitor.ip_address

    def get_user_agent(self, obj):
        return obj.visitor.user_agent

    def get_session_key(self, obj):
        return obj.visitor.session_key


admin.site.register(Visitor, VisitorAdmin)
admin.site.register(PageView, PageViewAdmin)
