import unicodedata


def get_sortkey(word):
    word = word.replace('œ', 'oe')
    return ''.join((c for c in unicodedata.normalize('NFD', word) if unicodedata.category(c) != 'Mn'))
