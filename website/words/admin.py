from django.contrib import admin
from django.db.utils import IntegrityError
from django.core.exceptions import ObjectDoesNotExist

from .similar import flexions
from .models import Word, Quotes, FrenchWords


class WordAdmin(admin.ModelAdmin):
    list_display = ('get_checked', 'get_occurr', 'word', 'quote')
    actions = ['remove_word', 'approve_word']

    def approve_word(self, request, queryset):
        """
        Allow neologisms to appear on the website.
        """
        for x in queryset:
            Word.objects.select_for_update().filter(word=x.word.word).update(checked=True)
            for y in flexions(x.word.word, checked=False):
                Word.objects.select_for_update().filter(word=y).update(checked=True)

    approve_word.short_description = "Allow selected words to appear on the website"

    def remove_word(self, request, queryset):
        """
        Permanently removes unverified neologisms.
        """
        for x in queryset:
            try:
                FrenchWords.objects.create(word=x.word.word)
                Word.objects.get(word=x.word.word).delete()
            except (IntegrityError, ObjectDoesNotExist):
                pass

    def get_checked(self, obj):
        return obj.word.checked

    get_checked.boolean = True
    get_checked.short_description = 'Checked'
    get_checked.admin_order_field = 'word__checked'

    def get_occurr(self, obj):
        return obj.word.occurr

    get_occurr.short_description = 'Occurrences'
    get_occurr.admin_order_field = 'word__occurr'

admin.site.register(Quotes, WordAdmin)
