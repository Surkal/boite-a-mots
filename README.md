## Description

Ce projet analyse des dizaines de sites d’actualités et blogs francophones afin de rassembler tous les néologismes.

## Détails techniques

Le projet est écrit en Python et développé avec Docker.

Les sites Internet sont parcourus et analysés avec Scrapy. Les données sont stockées avec PostgreSQL et mis en page avec Django.

La base de donnée des mots français se base sur le wiktionnaire francophone.

## Installation

Installer Docker

Créer une image docker

`docker build -t boite-a-mots .`


Configurer la base de donnée

`docker-compose run web python manage.py makemigrations words tracking`

`docker-compose run web python manage.py migrate`


Ajouter tous les mots français grâce aux dumps du wiktionnaire (étape la plus longue)

`docker-compose run web python french_words.py`


Lancer les 2 spiders

`docker-compose run scraper scrapy crawl rss`

`docker-compose run scraper scrapy crawl news`



Lancer le site en local

`docker-compose up`


Se rendre sur http://127.0.0.1:8000

## Aperçu

http://176.31.250.173
