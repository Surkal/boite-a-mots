$(function () {
  $("document").ready(function() {
    $(".new-words").on("mouseover", ChangeColorNW);
    $(".new-words").on("mouseout", ChangeBackColorNW);
    $(".examples").on("mouseover", ChangeColorEx);
    $(".examples").on("mouseout", ChangeBackColorEx);
    $(".websites").on("mouseover", ChangeColorWs);
    $(".websites").on("mouseout", ChangeBackColorWs);
    $("#selectPeriod").on("change", ChangePeriod);
  });
  function ChangeColorNW(evt) {
    $(".new-words").css("background-color", "#b8d4cf");
  }
  function ChangeBackColorNW(evt) {
    $(".new-words").css("background-color", "#cdece7");
  }
  function ChangeColorEx(evt) {
    $(".examples").css("background-color", "#7ea4c5");
  }
  function ChangeBackColorEx(evt) {
    $(".examples").css("background-color", "#8db7db");
  }
  function ChangeColorWs(evt) {
    $(".websites").css("background-color", "#b9cae1");
  }
  function ChangeBackColorWs(evt) {
    $(".websites").css("background-color", "#cee1fb");
  }
  function ChangePeriod(evt) {
    var period = $("#selectPeriod").val();
    var url = document.location.pathname + '?period=' + period;
    $.ajax({url: url,
            success: function(response) {
              $('.popular-words').html($(response).find('.popular-words').html());
            },
    });
    var seemore = '/words/index?show=popular&period=' + period;
    $('#seeMore').attr('href', seemore);
  }
});
