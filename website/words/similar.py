from django.core.exceptions import ObjectDoesNotExist

from words.models import Word


def flexions(word, checked=True):

    similar_words = []
    sim = []

    suffixes = (('teur', 'teurs', 'trice', 'trices'),
                ('eur', 'eurs', 'euse', 'euses'),
                ('eux', 'euse', 'euses'),
                ('ien', 'iens', 'ienne', 'iennes'),
                ('er', 'ers', 'ère', 'ères'),
                ('é', 'ée', 'és', 'ées'),
                ('al', 'aux', 'ale', 'ales'),
                ('o', 'i'),
                ('s', ''),
                ('e', ''))

    for suf in suffixes:
        for s in suf:
            if not word.endswith(s):
                continue
            for x in suf:
                if x == s:
                    continue
                if not s:
                    flex = word + x
                else:
                    flex = word[:-len(s)] + x  # Only for suffixes
                try:
                    y = Word.objects.get(word=flex, checked=checked)
                    sim.append(y)
                except ObjectDoesNotExist:
                    pass
        if len(sim) > len(similar_words):
            similar_words = sim
        sim = []
    return similar_words

def similar_words(word):
    similar_words = []
    for y in Word.objects.filter(sortkey=word.sortkey, checked=True):
        if y.word == word.word:
            continue
        similar_words.append(y)
    return similar_words
