# Basic docker image for nouveaux-mots
#
# Usage:
#   docker-compose up -d
#   docker exec nouveauxmots_db_1 createdb -U postgres words
#   docker-compose up --build -d

FROM python:3.6
MAINTAINER Surkal <https://gitlab.com/Surkal>

ENV C_FORCE_ROOT 1

# Create unprivileged user
RUN useradd docker

# Install PostgreSQL dependencies
RUN apt-get update && \
    apt-get install -y apt-utils postgresql-client libpq-dev memcached && \
    rm -rf /var/lib/apt/lists/*

ENV PYTHONBUFFERED 1

# Default port the webserver runs on
EXPOSE 8000

RUN mkdir /config

# Working directory for the application
ENV APPLICATION_ROOT /code/
RUN mkdir -p $APPLICATION_ROOT
WORKDIR $APPLICATION_ROOT

# Install required modules
ADD requirements.txt $APPLICATION_ROOT
RUN pip install -r requirements.txt
RUN ["python", "-c", "import nltk; nltk.download('stopwords', download_dir='/usr/local/share/nltk_data'); nltk.download('punkt', download_dir='/usr/local/share/nltk_data')"]

ADD . $APPLICATION_ROOT
