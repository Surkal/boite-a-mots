from django.test import TestCase

from words.utils import get_sortkey


class UtilsTest(TestCase):

    def test_sortkey(self):
        self.assertEqual(get_sortkey('hello'), 'hello')
        self.assertEqual(get_sortkey('Hello'), 'Hello')
        self.assertEqual(get_sortkey('héllo'), 'hello')
        self.assertEqual(get_sortkey('Héllo'), 'Hello')
        
