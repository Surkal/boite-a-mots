import unicodedata
from time import strftime

from faker import Factory
import django.utils.timezone
from django.urls import reverse
from django.test import TestCase, Client, RequestFactory

from words.views import WordView, StatsView
from words.models import Word, Quote, Quotes


class PopulatingTestCase(TestCase):

    def setUp(self):
        """Populate the models."""
        self.phrases = set()
        self.fake = Factory.create('fr_FR')
        for _ in range(3000):
            self.phrases.add(self.fake.catch_phrase())
        self.words = {'créateur': 3, 'créatrice': 3, 'créateurs': 3, 'créatrices': 3,
                      'testeur': 2, 'testeurs': 2, 'testeuse': 2,
                      'heureux': 2, 'heureuse': 2, 'heureuses': 2,
                      'miennes': 2, 'mien': 2, 'miens': 2, 'tienne': 1, 'tiens': 1,
                      'premier': 3, 'premiers': 3, 'première': 3, 'premières': 3,
                      'test': 1, 'tests': 1, 'spatial': 2, 'spatiaux': 2, 'spatiales': 2, 'animal': 1, 'animaux': 1,
                      'goléador': 1, 'goléadors': 1, 'goleador': 0,
                      'charmant': 1, 'charmante': 1,
                      'scudetto': 1, 'scudetti': 1,
                      'für': 0, 'fùr': 0,
                      'alone': 0,
                      'manyquotes': 0,
        }
        self.factory = RequestFactory()
        for w in self.words.keys():
            sortkey = ''.join((c for c in unicodedata.normalize('NFD', w) if unicodedata.category(c) != 'Mn'))
            a = Word.objects.create(word=w, sortkey=sortkey, checked=True)
            b = Quote.objects.create(quote=self.phrases.pop(),
                                     title=self.fake.sentence(),
                                     hour=self._random_hour(),
                                     url=self.fake.url(),
            )
            Quotes.objects.create(word=a, quote=b)

    def _random_hour(self):
        # Return a random date of maximum 30 days ago
        date =self.fake.date_time_between(start_date="-30d", end_date="now")
        return date.strftime("%Y-%m-%dT%H:%M:%S+0000")


class SimpleTest(PopulatingTestCase):
    def setUp(self):
        super().setUp()

    def test_homepage(self):
        self.assertEqual(self.client.get(reverse('homepage')).status_code, 200)

    def test_random_page(self):
        self.assertEqual(self.client.get(reverse('random')).status_code, 302)
        # Redirect to a word page
        redirected_url = self.client.get(reverse('random')).url
        self.assertTrue(any(redirected_url == reverse('word', kwargs={'title': x.word}) for x in Word.objects.all()))
