import os
import sys
import mock
import unittest

from faker import Factory
from scrapy.http import Request
from django.test import TestCase
from scrapy.exceptions import IgnoreRequest
from django.core.exceptions import ObjectDoesNotExist

sys.path.insert(0, '/code')

from words.models import AnalyzedUrls
from scraper.middlewares import IgnoreDuplicates


class IgnoreDuplicatesTest(TestCase):

    def setUp(self):
        self.fake = Factory.create('fr_FR')
        self.request = mock.MagicMock()
        self.spider = mock.MagicMock()
        self.spider.name = "TestSpider"
        self.ignore_duplicates = IgnoreDuplicates()

    def test_process_request(self):
        for _ in range(10):
            url = self.fake.url()
            self.assertEqual(self.ignore_duplicates.process_request(Request(url), self.spider), None)
            AnalyzedUrls.objects.create(url=url)
            with self.assertRaises(IgnoreRequest):
                self.ignore_duplicates.process_request(Request(url), self.spider)
