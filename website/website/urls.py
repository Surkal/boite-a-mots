from django.contrib import admin
from django.conf import settings
from django.conf.urls import include, url
from django.views.decorators.cache import cache_page

from .views import homepage

urlpatterns = [
    url(r'^$', cache_page(86400)(homepage), name="homepage"),
    url(r'^words/', include('words.urls')),
    url(r'^bureauduchef/', admin.site.urls),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
