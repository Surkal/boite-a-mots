from django.core.exceptions import ObjectDoesNotExist

from words.models import FrenchWords


class ExistingWord(object):
    """Analyze words."""
    def __init__(self, stopwords):
        self.stopwords = stopwords

    def is_word(self, sentence, words, word):
        self.sentence = sentence
        self.words = words
        self.word = word
        if not self.not_exist():
            return True
        if self.locution():
            return True
        if self._forbidden_char():
            return True
        return False

    def not_exist(self):
        if not self.word.isalpha() or not self.word.islower():
            return None
        if self.word in self.stopwords:
            return None
        if len(self.word) < 3:
            return None
        try:
            FrenchWords.objects.get(word=self.word)
        except ObjectDoesNotExist:
            if not self.locution():
                return True
        return None

    def locution(self):
        resultat = []
        position = self.words.index(self.word)
        for i in range(-2, 1):
            res = self.locution_exist_or_not(' '.join(self.words[position+i:position+i+3]))
            resultat.append(res)
            if not i == -2:
                res = self.locution_exist_or_not(' '.join(self.words[position+i:position+i+2]))
                resultat.append(res)
        if True in resultat:
            return True
        return False

    def locution_exist_or_not(self, loc):
        loc = loc.lower()
        loc_no_whitespace = loc.replace(" ", "")
        if not loc_no_whitespace.isalpha():
            return False
        try:
            FrenchWords.objects.get(word=loc)
        except ObjectDoesNotExist:
                return False
        return True

    def _forbidden_char(self):
        chars = ['@', '#', '©', '|', 'AFP', 'Pour lire ce contenu', '>>', 'Lire aussi :', 'ʺ']
        if any(c in self.sentence for c in chars):
            return True
        return False
