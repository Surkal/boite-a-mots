from random import randrange

from django.urls import reverse
from django.test import TestCase, Client

from .tests import PopulatingTestCase
from words.models import Word, Quote, Quotes


class WordTest(PopulatingTestCase):
    def setUp(self):
        super().setUp()
        # Create 120 quotes related to word 'manyquotes'
        for _ in range(120):
            w = Word.objects.get(word='manyquotes')
            q = Quote.objects.create(quote=self.phrases.pop(),
                                     title=self.fake.sentence(),
                                     hour=self._random_hour(),
                                     url=self.fake.url,
            )
            Quotes.objects.create(quote=q, word=w)

    def test_general_view(self):
        # Known words
        for w in Word.objects.all():
            self.assertEqual(self.client.get(reverse('word', kwargs={'title': w.word})).status_code, 200)
        # Unknown words
        for w in set(self.fake.word() for _ in range(20)):
            self.assertEqual(self.client.get(reverse('word', kwargs={'title': w})).status_code, 404)

    def test_context_paginated(self):
        r = self.client.get(reverse('word', kwargs={'title': 'manyquotes'}))
        self.assertTrue(r.context['is_paginated'])

    def test_different_pagination(self):
        for _ in range(5):
            i = randrange(5, 51)
            r = self.client.get(f'{reverse("word", kwargs={"title": "manyquotes"})}?pagesize=' + str(i))
            self.assertEqual(r.status_code, 200)
            self.assertEqual(len(r.context['object_list']), i)

    def test_flexions(self):
        for k, v in self.words.items():
            self.assertEqual(len(self.client.get(reverse('word', kwargs={'title': k})).context['flexions']), v)

    def test_flexions_not_checked(self):
        w1 = Word(word="bonjour", sortkey="bonjour", checked=True)
        w2 = Word(word="bonjours", sortkey="bonjours", checked=True)
        w3 = Word(word="bonjoures", sortkey="bonjoures", checked=False)
        w1.save()
        w2.save()
        w3.save()
        self.assertEqual(len(self.client.get(reverse('word', kwargs={'title': 'bonjour'})).context['flexions']), 1)

    def test_not_checked_word(self):
        # Not checked neologism must raise 404 error.
        w = Word(word="notchecked", sortkey="notchecked", checked=False)
        w.save()
        self.assertEqual(self.client.get(reverse("word", kwargs={"title": "notchecked"})).status_code, 404)
