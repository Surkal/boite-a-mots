SHELL:=/bin/bash

SCRIPT = docker-compose run web python manage.py

build:
	docker-compose build

runserver:
	docker-compose up

test:
	$(SCRIPT) test

shell:
	$(SCRIPT) shell

dbshell:
	$(SCRIPT) dbshell

backup:
	docker exec boiteamots_db_1 pg_dump -U postgres | gzip > backup-$(shell date "+%Y-%m-%d").tar.gz
