import os
import sys
import mock
import django
import unittest
import datetime

from scrapy.exceptions import DropItem

from scraper.pipelines import DatePipeline, TitlePipeline


class TestDatePipeline(unittest.TestCase):

    def setUp(self):
        self.spider = mock.MagicMock()
        self.spider.name = "TestSpider"
        self.pipeline = DatePipeline('17-02-28+0100')
        self.date = ['2017-05-05T05:05:05+0000',
                     '2017-05-05T05:05:05+00:00',
                     '2017-05-05T05:05:05',
                     '2017-05-05T05:05:05Z',
                     '05-05-2017T05:05:05Z-00:00',
                     '05-05-2017T05:05:05Z+00:00',
                     '2017-05-05T05:05',
                     '2017-05-05T05:05:05Z-00:00',
                     '2017-05-05T05:05:05Z+00:00',
                     '05/05/2017',
                     '2017-05-05',
                     '2017/05/05',
                     '2017-05-05CEST05:05:05+00:00',
                     '2017-05-05T05:05+00:00',
                     '1505050505',
                     '2017-05-05 05:05:05',
                     '20170505 05:05',
                     '16/10/2017 05:05:05 +00:00',
                     '16.10.2017',
                     '19/10/17 11h40 ',
                     '2018-03-16T14:52:50.000Z',
        ]

    def test_process_complete_item(self):
        item = {}
        for date in self.date:
            item['date'] = date
        self.assertEqual(self.pipeline.process_item(item, self.spider), item)

    def test_process_too_old_item(self):
        item = {"date": "2015-01-01T01:01:01+0000"}
        with self.assertRaises(DropItem):
            self.pipeline.process_item(item, self.spider)

    def test_process_empty_item(self):
        item = {}
        with self.assertRaises(KeyError):
            self.pipeline.process_item(item, self.spider)

    def test_unix_time(self):
        item = {'date': '1505050505'}
        d = self.pipeline.process_item(item, self.spider)
        self.assertIsInstance(d['date'], datetime.datetime)


class TestTitlePipeline(unittest.TestCase):

    def setUp(self):
        self.spider = mock.MagicMock()
        self.spider.name = "TestSpider"
        self.pipeline = TitlePipeline()

    def test_process_complete_item(self):
        item = {"title": "This is a title"}
        self.assertEqual(self.pipeline.process_item(item, self.spider), item)

    def test_process_empty_item(self):
        item = {}
        with self.assertRaises(KeyError):
            self.pipeline.process_item(item, self.spider)
