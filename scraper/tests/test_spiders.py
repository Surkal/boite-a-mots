import os
import sys
import unittest

import django
from scrapy.http import Response

DJANGO_PROJECT_PATH = '/code/website'
DJANGO_SETTINGS_MODULE = 'website.settings'
sys.path.insert(0, DJANGO_PROJECT_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = DJANGO_SETTINGS_MODULE
django.setup()

from scraper.spiders.news import NewsReader, RSSReader


class TestSources(unittest.TestCase):

    def test_available_sources(self):
        urls = NewsReader.sitemap_urls + RSSReader.start_urls
        self.assertGreater(len(urls), 0)  # urls not empty tab
        for url in urls:
            self.assertEqual(Response(url).status, 200)
