from django.urls import reverse
from django.test import TestCase
from django.db import IntegrityError

from .tests import PopulatingTestCase
from words.models import Word, Quote, Quotes, AnalyzedUrls, FrenchWords


class ModelTest(PopulatingTestCase):

    def setUp(self):
        super().setUp()

    def test_model_word(self):
        for word in Word.objects.all():
            self.assertEqual(str(word), word.word)
            self.assertEqual(word.get_absolute_url(), '/words/' + word.word)

    def test_model_quote(self):
        for quote in Quote.objects.all():
            self.assertEqual(str(quote), quote.quote)
            self.assertIsInstance(quote.date, str)
            self.assertEqual(len(quote.date), 10)

    def test_model_quotes(self):
        for x in Quotes.objects.all():
            self.assertEqual(str(x), x.quote.quote)

    def test_model_frenchwords(self):
        words = set(self.fake.word() for _ in range(10))
        for word in words:
            f = FrenchWords.objects.create(word=word)
            self.assertEqual(str(f), word)

    def test_model_analyzedurls(self):
        for _ in range(10):
            u = AnalyzedUrls.objects.create(url=self.fake.url())
            self.assertEqual(str(u), u.url)
