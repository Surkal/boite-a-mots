from django.urls import reverse
from django.test import TestCase

from visitors.models import Visitor, PageView


class MiddlewareTestCase(TestCase):

    def test_no_session(self):
        self.assertEqual(Visitor.objects.count(), 0)
        self.assertEqual(PageView.objects.count(), 0)

    def test_track_pageviews(self):
        self.client.get('/')
        self.assertEqual(Visitor.objects.count(), 1)
        self.assertEqual(PageView.objects.count(), 1)

    def test_track_user_agent(self):
        self.client.get('/', HTTP_USER_AGENT='django')
        self.assertEqual(Visitor.objects.count(), 1)
        visitor = Visitor.objects.get()
        self.assertEqual(visitor.user_agent, 'django')

    def test_track_referer_string(self):
        self.client.get('/?foo=bar&baz=bin', HTTP_REFERER='http://foo/bar')
        self.assertEqual(Visitor.objects.count(), 1)
        self.assertEqual(PageView.objects.count(), 1)
        view = PageView.objects.get()
        self.assertEqual(view.referer, 'http://foo/bar')

    def test_track_method_get(self):
        self.client.get('/')
        pageview = PageView.objects.get()
        self.assertEqual(pageview.method, 'GET')

    def test_track_method_post(self):
        self.client.post('/')
        pageview = PageView.objects.get()
        self.assertEqual(pageview.method, 'POST')

    def test_track_status_code_200(self):
        self.client.get('/')
        pageview = PageView.objects.get()
        self.assertEqual(pageview.status_code, 200)

    def test_track_status_code_404(self):
        self.client.get('invalid')
        pageview = PageView.objects.get()
        self.assertEqual(pageview.status_code, 404)

    def test_ignore_admin_urls(self):
        self.client.get('/bureauduchef/')
        self.assertEqual(Visitor.objects.count(), 1)
        self.assertEqual(PageView.objects.count(), 0)
        self.client.get('/bureauduchef/blah')
        self.assertEqual(Visitor.objects.count(), 2)
        self.assertEqual(PageView.objects.count(), 0)
