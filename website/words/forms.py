from django import forms


class AdvancedSearchForm(forms.Form):
    q = forms.CharField(label='Mot', required=False)
    start = forms.CharField(label='Commence par', required=False)
    end = forms.CharField(label='Se termine par', required=False)
    contain = forms.CharField(label='Contient', required=False)


class ExampleSearchForm(forms.Form):
    w = forms.CharField(label='Recherche ', required=False)


QUOTES_SORT_CHOICES = [
    ('rand', 'Aléatoirement'),
    ('desc', 'Du plus ancien au plus récent'),
    ('asc', 'Du plus récent au plus ancien'),
]

PAGE_SIZE = ((size, size) for size in (5, 10, 20, 50))
PERIOD = ((size, size) for size in (1, 3, 7, 14))


class QuotesSortForm(forms.Form):
    quotesort = forms.ChoiceField(choices=QUOTES_SORT_CHOICES,
                                  widget=forms.RadioSelect(),
                                  required=False,
                                  label='Organisation des exemples'
                                  )
    pagesize = forms.MultipleChoiceField(widget=forms.Select,
                                         choices=PAGE_SIZE,
                                         label='Résultats par page')
