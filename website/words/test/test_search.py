import urllib.parse

from faker import Factory
from django.urls import reverse
from django.test import TestCase, Client

from .tests import PopulatingTestCase
from words.models import Word, Quote, Quotes


class SearchTest(PopulatingTestCase):

    def setUp(self):
        super().setUp()

    def test_general_view(self):
        self.assertEqual(self.client.get(reverse('search')).status_code, 404)
        self.assertEqual(self.client.get(f'{reverse("search")}?q=python').status_code, 200)

    def test_search_exact_word(self):
        for _ in range(10):
            word = Word.objects.all().order_by('?').first().word
            r = self.client.get(f'{reverse("search")}?q=' + word)
            self.assertEqual(r.status_code, 302)
            self.assertEqual(r.url, reverse('word', kwargs={'title': word}))

    def test_search_sortkey_word(self):
        # createur -> créateur
        redirect = self.client.get(f'{reverse("search")}?q=createur')
        self.assertEqual(redirect.url, '/words/cr%C3%A9ateur')
        # premiére -> première
        redirect = self.client.get(f'{reverse("search")}?q=premiére')
        self.assertEqual(redirect.url, ('/words/premi%C3%A8re'))
        # fur -> für / fùr
        self.assertEqual(self.client.get(f'{reverse("search")}?q=fur').status_code, 200)

    def test_not_checked_word(self):
        Word.objects.create(word="notchecked", sortkey="notchecked", checked=False)
        self.assertEqual(self.client.get(f'{reverse("search")}?q=notchecked').status_code, 200)

    def test_search_close_words(self):
        # Several results
        r = self.client.get(f'{reverse("search")}?q=mianne')
        self.assertEqual(r.status_code, 200)
        self.assertTrue(len(r.context['suggested_words']) > 1)
        # Unique result
        r = self.client.get(f'{reverse("search")}?q=allone')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.context['suggested_words']), 1)

    def test_advanced_search(self):
        r = self.client.get(reverse('advanced_search'))
        self.assertEqual(r.status_code, 200)

    def test_search_start(self):
        # 1 word begin by 'alon'
        r = self.client.get(f'{reverse("search")}?start=alon&end=&contain=')
        self.assertEqual(len(r.context['suggested_words']), 1)
        self.assertEqual(r.status_code, 200)
        # 3 words begin by 'gol'
        r = self.client.get(f'{reverse("search")}?start=gol&end=&contain=')
        self.assertEqual(len(r.context['suggested_words']), 3)
        self.assertEqual(r.status_code, 200)
        # JS not enabled and/or wrong search
        r = self.client.get(f'{reverse("search")}?q=badquery&start=gol&end=&contain=')
        self.assertEqual(r.status_code, 200)

    def test_search_end(self):
        # 1 word ends by 'réatrice'
        r = self.client.get(f'{reverse("search")}?start=&end=réatrice&contain=')
        self.assertEqual(len(r.context['suggested_words']), 1)
        self.assertEqual(r.status_code, 200)
        # Many words ends by 's'
        r = self.client.get(f'{reverse("search")}?start=&end=s&contain=')
        self.assertTrue(len(r.context['suggested_words']) > 1)
        self.assertEqual(r.status_code, 200)

    def test_search_contain(self):
        # 1 word contains 'lon'
        r = self.client.get(f'{reverse("search")}?start=&end=&contain=lon')
        self.assertEqual(len(r.context['suggested_words']), 1)
        self.assertEqual(r.status_code, 200)
        # Many words contains by 'e'
        r = self.client.get(f'{reverse("search")}?start=&end=&contain=e')
        self.assertTrue(len(r.context['suggested_words']) > 1)
        self.assertEqual(r.status_code, 200)

    def test_search_paginate(self):
        r = self.client.get(f'{reverse("search")}?start=gol&end=&contain=&paginateby=100')
        self.assertEqual(r.status_code, 200)
        r = self.client.get(f'{reverse("search")}?start=gol&end=&contain=&paginateby=1')
        self.assertEqual(r.status_code, 200)


class SearchQuoteTest(TestCase):

    def setUp(self):
        self.fake = Factory.create('fr_FR')
        self.populate()

    def populate(self):
        a = Word.objects.create(word="pythonist", sortkey="pythonist", checked=True)
        b = Word.objects.create(word="awesome", sortkey="awesome", checked=True)
        c = Word.objects.create(word="ridiculous", sortkey="ridiculous", checked=False)
        a_ = Quote.objects.create(quote="I wanna be a pythonist.", title=self.fake.sentence(), url=self.fake.url())
        b_ = Quote.objects.create(quote="Python is awesome.", title=self.fake.sentence(), url=self.fake.url())
        c_ = Quote.objects.create(quote="To code in python is not ridiculous.", title=self.fake.sentence(), url=self.fake.url())
        Quotes.objects.create(word=a, quote=a_)
        Quotes.objects.create(word=b, quote=b_)
        Quotes.objects.create(word=c, quote=c_)

    def test_general_view(self):
        self.assertEqual(self.client.get(reverse('search_quote')).status_code, 404)
        self.assertEqual(self.client.get(f'{reverse("search_quote")}?w=python').status_code, 200)

    def test_search_result(self):
        r = self.client.get(f'{reverse("search_quote")}?w=python')
        self.assertEqual(len(r.context['object_list']), 2)
