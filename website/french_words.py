import os
import sys
import bz2
import urllib.request
import xml.etree.ElementTree as ET

import django
from django.db import IntegrityError
from django.db.utils import DataError


DJANGO_PROJECT_PATH = '/code/website'
DJANGO_SETTINGS_MODULE = 'website.settings'
sys.path.insert(0, DJANGO_PROJECT_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = DJANGO_SETTINGS_MODULE
django.setup()

from words.models import FrenchWords

# Download the latest french wiktionary dump in temporary files
url = 'https://dumps.wikimedia.org/frwiktionary/latest/frwiktionary-latest-pages-articles.xml.bz2'
filename, headers = urllib.request.urlretrieve(url)

# Analyze
bzfile = bz2.BZ2File(filename)

count = 0
for event, elem in ET.iterparse(bzfile, events=('start', 'end', 'start-ns', 'end-ns')):
    if event == 'end':
        if 'title' in elem.tag:
            title = elem.text
        if 'text' in elem.tag:
            if elem.text:
                if '{{langue|fr}}' in elem.text:
                    count += 1
                    print(count)
                    try:
                        FrenchWords.objects.create(word=title)
                    except (IntegrityError, DataError):
                        pass
        elem.clear()  # Avoid memory leaks
