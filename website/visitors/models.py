from django.utils import timezone
from django.contrib.gis.geoip2 import GeoIP2
from geoip2.errors import AddressNotFoundError

from django.db import models

class Visitor(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40, editable=False, blank=True)
    ip_address = models.CharField(max_length=39, editable=False, blank=True)
    user_agent = models.TextField(null=True, editable=False)
    start_time = models.DateTimeField(default=timezone.now, editable=False)

    class Meta(object):
        ordering = ('-start_time',)

    @property
    def country_name(self):
        g = GeoIP2()
        try:
            country = g.country(self.ip_address)['country_name']
        except AddressNotFoundError:
            country = ''
        return country


class PageView(models.Model):
    visitor = models.ForeignKey(
        Visitor,
        related_name='pageviews',
        on_delete=models.CASCADE,
    )
    url = models.URLField(null=False, editable=False)
    referer = models.TextField(null=True, editable=False)
    view_time = models.DateTimeField(default=timezone.now, editable=False)
    method = models.CharField(max_length=20, editable=False, null=True)
    status_code = models.SmallIntegerField(null=True, editable=False)

    class Meta(object):
        ordering = ('-view_time',)
