import re

from django.core.exceptions import ObjectDoesNotExist

from visitors.utils import get_ip_address
from visitors.models import Visitor, PageView

class VisitorMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        visitor = self._refresh_visitor(request)
        response = self.get_response(request)

        #if not self._should_track(request, response):
            #return response

        self._add_pageview(visitor, request, response)
        return response

    def _should_track(self, request, response):
        pass

    def _refresh_visitor(self, request):
        session_key = request.session.session_key
        if not session_key:
            request.session.save()
        try:
            visitor = Visitor.objects.get(pk=session_key)
        except ObjectDoesNotExist:
            ip_address = get_ip_address(request)
            user_agent = request.META.get('HTTP_USER_AGENT', None)
            visitor = Visitor(
                pk=session_key,
                ip_address=ip_address,
                user_agent=user_agent,
            )
            visitor.save()
        return visitor

    def _add_pageview(self, visitor, request, response):
        referer = request.META.get('HTTP_REFERER', None)
        url = request.path

        # Do not track admin urls
        path = re.compile(r'/bureauduchef/')  # TODO: too hard coded
        if path.match(request.path):
            return False

        pageview = PageView(
            visitor=visitor,
            referer=referer,
            url=url,
            method=request.method,
            status_code=response.status_code,
        )
        pageview.save()
